#  pyCoDa

[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)


pyCoDa provides compositional data (CoDa) analysis tools for Python

- **Source code:** https://bitbucket.org/genomicepidemiology/pycoda

## Getting Started

This package extends the Pandas dataframe object with various CoDa tools. It also provides a set of plotting functions for CoDa figures.

### Installation

Clone the git repo to your local hard drive:

    git clone git@bitbucket.org:genomicepidemiology/pycoda.git

Enter pycoda directory and type

    pip install ./

### Usage

The pyCoDa module is loaded as

    import pycoda

At this point, in order to get CLR values from a Pandas DataFrame df, do

    df.coda.clr()


## Documentation

### CLR transformation - point estimate
    df.coda.clr()

Returns centered logratio coefficients. If the data frame contains zeros, values
will be replaced by the Aitchison mean point estimate.

### CLR transformation - standard deviation
    df.coda.clr_std(n_samples=5000)

Returns the standard deviation of n_samples random draws in CLR space.

**Parameters**

- n_samples (int) - Number of random draws from a Dirichlet distribution.


### ALR transformation - point estimate
    df.coda.alr(part=None)

Same as clr() but returning additive logratio values. If part is None, then the last part of the composition is used, otherwise part is used as denominator.

**Parameters**

- part (str) - Name of the part to be used as denominator.   


### ALR transformation - standard deviation
    df.coda.alr_std(part=None, n_samples=5000)

Same as clr_std, but in ALR space.

**Parameters**

- part (str) - Name of the part to be used as denominator.   

- n_samples (int) - Number of random draws from a Dirichlet distribution.


### ILR transformation - point estimate
    df.coda.ilr(psi=None)

Same as clr() but for isometric logratio transform. An orthonormal basis can be
provided as psi. If no basis is given, a default sequential binary partition basis will be used.

**Parameters**

- psi (array_like) - Orthonormal basis.

### ILR transformation - standard deviation
    df.coda.ilr_std(psi=None, n_samples=5000)

This method does not exist (yet).


### Bayesian zero replacement
    df.coda.zero_replacement(n_samples=5000)

Returns a count table with zero values replaced by finite values using Bayesian inherence.

**Parameters**

- n_samples (int) - Number of random draws from a Dirichlet distribution.


### Closure
    df.coda.closure(N)

Apply closure to constant N to the composition.

**Parameters**

- N (int) - Closure constant.

### Total variance
    df.coda.totvar()

Calculates the total variance of a set of compositions.

### Geometric mean
    df.coda.gmean()

Calculates the geometric mean of a set of compositions.

### Centering
    df.coda.center()

Centers the composition by dividing by the geometric mean.

### Scaling
    df.coda.scale()

Scales the composition by powering the composition by the inverse square-root of the total variance.


## Plotting functions

### PCA biplot
    pycoda.plot.pca()

### ILR coordinate plot
    pycoda.plot.ilr()

### Ternary diagram
    pycoda.plot.ternary()
