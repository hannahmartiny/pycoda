adjustText==0.7.3
matplotlib==3.1.1
numpy==1.17.2
pandas==0.25.1
pyCoDa==0.3
python-ternary==1.0.6
scipy==1.3.1
seaborn==0.9.0
