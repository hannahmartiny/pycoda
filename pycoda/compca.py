import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pycoda import extra
from matplotlib.patches import Patch
import warnings

filled_markers = ('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X')

def pca(data, parts, metadf, left_on=None, left_index=False):
    
    # replace zeroes
    df = data[parts].coda.zero_replacement()
    
    # close
    df = df.coda.closure(100)
      
    # clr transform
    df_clr = df.coda.center().coda.clr()
    
    # run SVD
    scores, eig_val, loadings = np.linalg.svd(df_clr)
    n_eig = len(eig_val)
    
    # explained variance by each pc
    explained_variance = ( eig_val ** 2 / np.sum(eig_val ** 2)) * 100
    explained_variance = pd.DataFrame(
        explained_variance,
        index=[f'PC{i+1}' for i in range(n_eig)],
        columns=['Explained Variance']
    )
    #explained_variance.columns.name = 'Variance'
    explained_variance.index.name = 'Principal Component'

    # make scores df
    scores = pd.DataFrame(
        scores.T,
        columns=df_clr.index
    )

    # loadings df
    # take inner product of eig_val * identity matrix of number of eigenvalues
    loadings = pd.DataFrame(
        np.inner(
            eig_val * np.identity(n_eig),
            loadings.T[0:n_eig, 0:n_eig]
        ),
        columns=df_clr.columns[0:n_eig],
        index=[f'PC{i+1}' for i in range(n_eig)]
    )

    scales = [np.max(np.abs(loadings.values)),
                  [np.max(np.abs(scores.loc[idx].values)) for idx in [1, 2]]]


    scores = scores[0:2]
    scores = (scales[0] * (scores.T / scales[1])).T
    scores_ext = scores.T.merge(metadf, right_index=True, left_index=left_index, left_on=left_on)
    
    return explained_variance, scores, loadings, scales, scores_ext


def plot_scree(explained_variance, ax, cumulative=False):
    """Create scree plot on ax

    Parameters
    ----------
    explained_variance : pd.DataFrame
        Dataframe with index as PCs and first column with values
    ax : matplotlib.axes
        Ax to plot on
    cumulative : bool, optional
        Add cumulative variance explained for each PC tick, by default False
    """

    rects = ax.bar(
        explained_variance.index,
        explained_variance.values[:, 0]
    )

    # annotate
    for rect in rects:
        height = rect.get_height()
        ax.annotate(
            f"{height:.1f}%",
            xy=(rect.get_x() + rect.get_width() / 2, height),
            xytext=(0, 3),
            textcoords="offset points",
            ha='center',
            va='bottom',
            fontsize=9
        )
    
    # cumulative line
    if cumulative:
        zs = np.cumsum(explained_variance.values[:, 0])
        ax.plot(
            explained_variance.index, zs, '.-',
            color='red',
            alpha=.5,
            label='Cumulative variance explained'
        )
        ax.legend()
    
    ax.set_xlabel('Principal Component', fontsize=12)
    ax.set_ylabel('Explained variance (%)', fontsize=12)


def plot_scores(ax, scores, x, y, hue=None, hue_order=None, style=None, style_order=None, **args):

    if hue is not None and hue_order is None:
        hue_order = sorted(scores[hue].unique())
    
    if style is not None and style_order is None:
        style_order = sorted(scores[style].unique())

    sns.scatterplot(
        data = scores,
        x = x, y = y,
        hue = hue, hue_order = hue_order,
        style = style, style_order = style_order,
        ax = ax,
        markers=filled_markers,
        **args
    )


def plot_arrows(ax, loadings, scale, x='PC1', y='PC2'):
    for idx, column in enumerate(loadings):
        # create arrow
        ax.arrow(
            0, 0, # starting point
            loadings.loc[x][idx], loadings.loc[y][idx], # end points
            facecolor='grey',
            alpha=.7,
            lw=0,
            width = scale * 0.004
        )

        # annotate arrow
        ax.annotate(
            column,
            (loadings.loc[x][idx] + .005, loadings.loc[y][idx]),
            color='black',
            ha='left',
            va='bottom',
            alpha=.8,
            fontsize=10
        )


def generate_colorscheme(groups):
    cm = plt.cm.tab20(np.linspace(0, 1, len(groups)))
    colors = {}
    for idx, group in enumerate(groups):
        colors[group] = cm[idx]
    return colors


def ellipse_legend(colors, ax, **args):
    
    legend_elements = []
    for label, color in colors.items():
         legend_elements.append(
             Patch(
                 facecolor=color, 
                 edgecolor=color,
                label=label
             )
         )
    ax.legend(
        handles=legend_elements, 
        **args
    )


def plot_ellipses(ax, scores, groupcol=None, legend_args={}):


    if groupcol is None: 
        warnings.warn("Argumenet for ellipse coloring is None, please provide a column name. Not creating ellipses.")
    
    else:
        ellipse_colors = generate_colorscheme(scores[groupcol].unique())

        for g, gdata in scores.groupby(groupcol):
            if gdata.shape[0] > 3:
                ellipse = extra.get_covariance_ellipse(gdata[[0, 1]])
            
                extra.plot_covariance_ellipse(ax, ellipse, color=ellipse_colors[g])
        
        
        # fix legends
        leg_fontsize = ax.legend().get_texts()[0].get_fontsize()
        leg1 = ax.legend(fontsize=leg_fontsize)
        ellipse_legend(ellipse_colors, ax=ax, fontsize=leg_fontsize, title_fontsize=leg_fontsize, **legend_args)
        ax.add_artist(leg1)
    
    
def plot_pca(explained_variance, loadings, scores, scales, scree=True, plotscores=True, plotscores_kwargs={}, plotellipses=True, ellipse_color=None, ellipselegend_kwargs={}, figsize=(10,7), cumulative=False):
    
    # initialize plotting
    # if scree, add two subplots and plot scree
    if scree:
        fig, axes = plt.subplots(
            1,2, 
            figsize=figsize,
            gridspec_kw={
                'width_ratios': [.6, 2]
            }
        )

        ax_scree, ax = axes.flatten()
        plot_scree(explained_variance, ax=ax_scree, cumulative=cumulative)

    else:
        fig, ax = plt.subplots(figsize=figsize)
    
    # create arrows
    plot_arrows(
        ax=ax,
        loadings=loadings,
        scale=scales[0],
        x='PC1',
        y='PC2'
    )

    # create scores scatter plot
    if plotscores:
        plot_scores(
            ax=ax,
            scores=scores,
            x=0,
            y=1,
            **plotscores_kwargs
        )
    
    # create ellipses plot
    if plotellipses:
        plot_ellipses(
            ax=ax,
            scores=scores,
            groupcol=ellipse_color,
            legend_args=ellipselegend_kwargs
        )
    
    # pretty biplot
    ax.set_xlabel(r'PC1 ({0:.1f}% explained variation)'.format(
        explained_variance.loc['PC1', 'Explained Variance']), fontsize=12)
    ax.set_ylabel(r'PC2 ({0:.1f}% explained variation)'.format(
        explained_variance.loc['PC2', 'Explained Variance']), fontsize=12)
    ax.set_xlim(-scales[0]*1.1, scales[0]*1.1)
    ax.set_ylim(-scales[0]*1.1, scales[0]*1.1)
    ax.plot([ax.get_xlim()[0], ax.get_xlim()[1]],
                [0.0, 0.0], '--', color='black', alpha=0.4)
    ax.plot([0.0, 0.0], [ax.get_ylim()[0], ax.get_ylim()[1]],
                '--', color='black', alpha=0.4)

    plt.close(fig)
        
    return fig