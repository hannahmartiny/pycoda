# -*- coding: utf-8 -*-
''' Compositional plots
'''

__author__ = "Christian Brinch"
__copyright__ = "Copyright 2019"
__credits__ = ["Christian Brinch"]
__license__ = "AFL 3.0"
__version__ = "0.5"
__maintainer__ = "Christian Brinch"
__email__ = "cbri@food.dtu.dk"

from importlib import reload
import ternary as td
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pycoda import extra
import seaborn as sns

reload(extra)


def _scree_plot(axis, eig_val):
    axis.set_xlabel('Component')
    axis.set_ylabel('Explained varaince')
    axis.set_xlim(0, min(len(eig_val)+1, 20))
    axis.bar(np.arange(len(eig_val))+1, (eig_val/np.sum(eig_val))**2)
    csum = np.cumsum(eig_val**2/np.sum(eig_val**2))
    for i in range(min(5, len(eig_val))):
        axis.annotate(str(np.round(csum[i]*100))+'%',
                      (i+1.2, (eig_val[i]/np.sum(eig_val))**2))


def _plotloadings(axis, loadings, plotloadings, scale, pcs=['pc1', 'pc2']):
    loadingnames = [plotloadings]
    if plotloadings in ['labels', 'both']:
        loadingnames = loadings.columns
    for idx, column in enumerate(loadings):
        axis.arrow(0, 0,
                   loadings.loc[pcs[0]][idx],
                   loadings.loc[pcs[1]][idx],
                   facecolor='grey',
                   alpha=0.7,
                   lw=0,
                   width=scale*0.008)
        if column in loadingnames and np.sqrt(pow(loadings.loc[pcs[0]][idx], 2)
                                              + pow(loadings.loc[pcs[1]][idx], 2)) > 1.:
            axis.annotate(column, (loadings.loc[pcs[0]][idx]+0.005,
                                   loadings.loc[pcs[1]][idx]),
                          color='black',
                          ha='left',
                          va='bottom',
                          alpha=0.8,
                          fontsize=12)


def _plotscores(axis, scores, descr, legend, labels, plotellipses, colors, pcs=['pc1', 'pc2']):
    if not colors and descr is not None:
        temp = plt.cm.tab20(np.linspace(0, 1, len(set(descr))))
        colors = {}
        for idx, group in enumerate(set(descr)):
            colors[group] = temp[idx]

    if descr is not None:
        for group in set(descr):
            # if group in ['Europe & Central Asia', 'Sub-Saharan Africa']:
            idx = descr.loc[descr == group].index
            axis.plot(*scores[idx].values, '.', alpha=0.7,
                      label=group, color=colors[group])

            if plotellipses and len(idx) > 3:
                ellipse = extra.get_covariance_ellipse(pd.DataFrame(scores[idx].values.T),
                                                       conf=95)
                extra.plot_covariance_ellipse(axis, ellipse, color=colors[group])

                if labels == 'sparse':
                    _ = [axis.text(scores.loc[pcs[0]][point]+0.005,
                                   scores.loc[pcs[1]][point], point,
                                   fontsize=10, alpha=0.8)
                         for point in idx
                         if extra.check_point_in_ellipse(scores[point], ellipse)]

            if labels == 'all':
                _ = [axis.text(scores.loc[pcs[0]][i]+0.005,
                               scores.loc[pcs[1]][i], i,
                               fontsize=10, alpha=0.8) for i in idx]
            if legend:
                axis.legend(frameon=False, markerscale=2.5)
    else:
        axis.plot(*scores.values, 'o', alpha=0.7, color='steelblue')


def _svd(clr):
    scores, eig_val, loadings = np.linalg.svd(clr)
    scores = pd.DataFrame(scores.T[0:2, :], columns=clr.index, index=['pc1', 'pc2'])
    loadings = pd.DataFrame(np.inner(eig_val*np.identity(len(eig_val)),
                                     loadings.T[0:len(eig_val), 0:len(eig_val)])[0:2],
                            columns=clr.columns[0:len(eig_val)], index=['pc1', 'pc2'])
    return scores, eig_val, loadings


def pca(data, descr=None, scree=True, labels=None, colors=None, plotloadings=None,
        plotellipses=True, legend=True, figsize=(10,7)):
    """Plot a PCA biplot
    
    Parameters
    ----------
    data : pd.DataFrame
        Compositional data set.
    descr : column, optional
        Labels for how each marker is colored, by default None
    scree : bool, optional
        If True, create the scree plot as well, by default True
    labels : str, optional
        If labels='all', all labels are drawn (rays and markers). If labels='sparse', draw labels only for long rays., by default None
    colors : [type], optional
        [description], by default None
    plotloadings : [type], optional
        [description], by default None
    plotellipses : bool, optional
        [description], by default True
    legend : bool, optional
        [description], by default True
    figsize : tuple, optional
        [description], by default (10,7)
    """

    scores, eig_val, loadings = _svd(data.coda.center().coda.clr())

    fig = plt.figure(1, figsize=figsize)
    plt.clf()
    if scree:
        frames = [[0.08, 0.08, .22, .90], [0.35, 0.08, .63, .90]]
        axis = (fig.add_axes(frames[0], frame_on=False), fig.add_axes(frames[1], frame_on=True))
        _scree_plot(axis[0], eig_val)
        p = 1
    else:
        frames = [[0.08, 0.08, .90, .90]]
        axis = (fig.add_axes(frames[0], frame_on=True))
        axis = [axis]
        p = 0

    scales = [np.max(np.abs(loadings.values)),
              [np.max(np.abs(scores.loc[idx].values)) for idx in ['pc1', 'pc2']]]

    # Setup biplot axis
    axis[p].set_xlabel(r'P.C. 1 ({0:.1f}% explained variation)'.format(
        np.round(eig_val[0]**2 / np.sum(eig_val**2)*100)))
    axis[p].set_ylabel(r'P.C. 2 ({0:.1f}% explained variation)'.format(
        np.round(eig_val[1]**2 / np.sum(eig_val**2)*100)))
    axis[p].set_xlim(-scales[0]*1.1, scales[0]*1.1)
    axis[p].set_ylim(-scales[0]*1.1, scales[0]*1.1)
    axis[p].plot([axis[p].get_xlim()[0], axis[p].get_xlim()[1]],
                 [0.0, 0.0], '--', color='black', alpha=0.4)
    axis[p].plot([0.0, 0.0], [axis[p].get_ylim()[0], axis[p].get_ylim()[1]],
                 '--', color='black', alpha=0.4)

    if plotloadings is not None:
        _plotloadings(axis[p], loadings, plotloadings, scales[0])

    scores = (scales[0]*(scores.T/scales[1])).T
    _plotscores(axis[p], scores, descr, legend, labels, plotellipses, colors)


def ilr_coords(data, descr=None, center=False, figsize=(10,7)):
    """Plot ILR coordinates
    
    Parameters
    ----------
    data : pd.DataFrame
        Compositional data set to be ilr transformed.
    descr : pd.DataFrame, optional
        Change coloring to represent different data groups. by default None.
    center : bool, optional
        Center the compositional data set before ILR transformation. by default False.
    figsize : tuple, optional
        Size of figure. by default (10,7)
    
    Raises
    ------
    AttributeError
        The compositional data set must be closed to 100.
    """
    if (abs(data.sum(1)-100.) > 1e-6).any():
        raise AttributeError("Error: Composition is not closed to 100.")

    if center:
        ilr = ((data/data.coda.gmean()).coda.closure(100)).coda.ilr().loc[:, [0, 1]]
    else:
        ilr = data.coda.ilr().loc[:, [0, 1]]

    bound = np.max([ilr.max().max(), -ilr.min().min()])*1.5

    fig = plt.figure(1, figsize=figsize)
    plt.clf()
    axis = plt.subplot()
    axis.set_xlabel('ILR $x_1^*$')
    axis.set_ylabel('ILR $x_2^*$')
    axis.set_xlim(-bound, bound)
    axis.set_ylim(-bound, bound)
    axis.set_aspect('equal')
    axis.plot([0, 0], [-bound, bound], '--', color='black', lw=0.5)
    axis.plot([-bound, bound], [0, 0], '--', color='black', lw=0.5)

    if descr is not None:
        for group in set(descr):
            idx = descr.loc[descr == group].index
            axis.plot(*ilr.loc[idx].T.values, 'o', alpha=0.7,
                      label=group)  # , color=descr.loc[idx[0], 'color'])
        axis.legend(frameon=False)
    else:
        axis.plot(*ilr.T.values, 'o', alpha=0.7, color='steelblue')

    points = extra.get_covariance_ellipse(ilr)
    extra.plot_covariance_ellipse(axis, points)


def compositional_line(data, tax, center=False):
    
    data = data.coda.closure(100)
    gm = data.coda.gmean()

    data_clr = data.coda.clr()
    _, _, loadings = np.linalg.svd(data_clr)
    loadings = pd.DataFrame(loadings)

    eigenvector = loadings[0].values # TODO: check if this is the correct axis.
    
    alphas = np.arange(-100, 100, 0.1).reshape(-1, 1)
    ys = pd.DataFrame(
        (np.exp(eigenvector) ** alphas) * gm
    )
    
    if center:
        ys = ys / ys.coda.gmean()
    ys = ys.coda.closure(100)
    

    tax.plot(ys.values, color='black', lw=2, alpha=.7, label='PC1')


def ternary(data, descr=None, center=False, conf=False, line=None, colors=None, size_inches=7, returnable=False):
    """Plot ternary diagram
    
    Parameters
    ----------
    data : pd.DataFrame
        compositional data frame
    descr : pd.DataFrame, optional
        Change coloring to represent different data groups, By default None.
    center : bool, optional
        If true, center the data before plotting. By default False.
    conf : bool, optional
        If standard deviations are known, add an eclipse around each sample corresponding to the confidence. By default False
    line : str, optional
        If line='pca', then the compositional line of the first principal component is added to the plot. By default None.
    colors : pd.DataFrame, list, array, optional
        Specify which colors should be used. By default None.
    size_inches : integer, optional
        Set width and height of the ternary figure. Value is in inches. By default 7.
    returnable : bool, optional
        If true, return the figure. By default False.
    """ 
    if np.shape(data)[1] > 3:
        raise AttributeError("Error: Too many parts in composition (max. 3).")
    for column in data.T:
        print(np.abs(data.T[column].sum()-100.))
        if np.abs(data.T[column].sum()-100.) > 1e-6:
            raise AttributeError("Error: Composition is not closed to 100.")

    figure, tax = td.figure(scale=100)
    figure.set_size_inches(size_inches, size_inches)
    tax.boundary(linewidth=1.5)
    tax.gridlines(color="blue", multiple=10, linewidth=0.5, alpha=0.5)
    tax.bottom_axis_label("% {0:s}".format(data.columns[0]), fontsize=16, offset=0.14)
    tax.right_axis_label("% {0:s}".format(data.columns[1]), fontsize=16, offset=0.14)
    tax.left_axis_label("% {0:s}".format(data.columns[2]), fontsize=16, offset=0.12)
    tax.ticks(axis='lbr', linewidth=1, multiple=10, offset=0.03)
    tax.clear_matplotlib_ticks()
    tax.get_axes().axis('off')

    if center:
        sdata = (data/data.coda.gmean()).coda.closure(100)
    else:
        sdata = data

    if descr is not None:
        # create a list of colors if none is given
        if colors is None:
            cm = plt.get_cmap('nipy_spectral')
            NUM_COLORS = descr.nunique()
            colors = [cm(i//3*3.0/NUM_COLORS) for i in range(NUM_COLORS)]

        for i, group in enumerate(set(descr)):
            idx = descr.loc[descr == group].index
            tax.scatter(sdata.loc[idx, :].values, alpha=0.7, label=group, color=colors[i])
    else:
        tax.scatter(sdata.values, alpha=0.7,
                    color='steelblue')

    psi = np.array([[1, 1, -1], [1, -1, 0]])
    psip = np.array([psi[i]/np.linalg.norm(psi[i]) for i in [0, 1]])

    if conf:
        ilr = sdata.coda.ilr().loc[:, [0, 1]]
        points = extra.get_covariance_ellipse(ilr)
        psi = extra.sbp_basis(sdata)
        ellipse = pd.DataFrame(np.exp(np.matmul(points, psi))).coda.closure(100)
        ellipse = ellipse.loc[:, [ellipse.columns[1], ellipse.columns[0],
                                  ellipse.columns[2]]]
        tax.plot(ellipse.values, color='black', lw=0.5, ls='-')

    if line == 'pca':
        compositional_line(data, tax, center=center)

    plt.tight_layout()
    artist=tax.legend(loc='top left')

    if returnable:
        plt.close(figure) 
        return figure

def multiple_ilr_coords(data, label=None, figsize=(10,7), connect=False):
    """Plot multiple sets of ILR data coordinates
    
    Parameters
    ----------
    data : pd.DataFrame or list of pd.DataFrames
        The set(s) of ILR coordinates
    label : list, optional
        If only one set of ILR coordinates the list of labels must match the number of rows in the data frame. 
        If two or more data sets are given, the length of the list must match the number of data sets. Each set will then get a different label.
        By default None.
    figsize : tuple, optional
        Size of figure, by default (10,7)
    connect : bool, optional
        If True and two data sets are given, then a line will be drawn that connects the coordinates row-wise in both sets.
        This can quite useful to see the effect of data permutation.
        By default False
    
    Returns
    -------
    fig, ax: plt.figure and plt.axis
        the ILR coordinate figure and axes
    
    """

    # set up the figure 
    fig, ax = plt.subplots(figsize=figsize)
    ax.axhline(0, color='grey', alpha=.7)
    ax.axvline(0, color='grey', alpha=.7)
    ax.set_xlabel('ILR $x_1^*$')
    ax.set_ylabel('ILR $x_2^*$')
    
    if label is not None:
        assert isinstance(label, list), "Please give the labels as a list"

    if isinstance(data, list): #compare two or more sets of ILR coordinates
        N_sets = len(data)
        
        if label is None: 
            label=np.arange(1, N_sets+1)

        colors=sns.color_palette("deep", N_sets)
        
        if connect and N_sets == 2:
            for row1, row2 in zip(data[0].values, data[1].values):
                points = np.vstack((row1, row2))
                ax.plot(points[:, 0], points[:,1], color='black', alpha=.7)
                
        for i in range(N_sets):
            sns.scatterplot(data=data[i], x=0, y=1, ax=ax, color=colors[i], label=label[i])
        
        plt.legend(title='Data sets', fontsize=12)

    elif isinstance(data, pd.DataFrame):
        data = data.copy()
        if label is not None and len(label) == data.shape[0]:
            data['label'] = label
        else:
            data['label'] = 'ilr data'
        sns.scatterplot(data=data, x=0, y=1, ax=ax, hue='label')

    else:
        raise NotImplementedError(
            'Not possible to create a ILR coordinate plot with data of type:{}'.format(type(data))
        )
    
    plt.close(fig)
    
    return fig, ax
