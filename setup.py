#!/usr/bin/env python
''' Setup file for pyCoDa
'''

from distutils.core import setup

setup(name='pyCoDa',
      version='0.5',
      description='Compositional Data Analysis in Python',
      author='Christian Brinch',
      author_email='cbri@food.dtu.dk',
      packages=['pycoda']
      )
